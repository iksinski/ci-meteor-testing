import expect from 'expect';

module.exports = function () {
    this.Given(/^I see title/, function () {
        var title = browser.getTitle();
        expect(title).toEqual('Google');
        browser.pause(10000)
    });
};