module.exports = {
    webdriverio: {
        desiredCapabilities: {
            browserName: "chrome",
            chromeOptions: {args: ["--no-sandbox"]}
        }
    },
    ddp: "http://localhost:3000",
    path: "tests",
    log: 'info',
    debug: true,
    seleniumDebug: true,
    debugCucumber: true,
    debugBrkCucumber: true,
    debugMocha: true,
    debugBrkMocha: true
};
