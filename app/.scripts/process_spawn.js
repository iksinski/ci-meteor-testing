const spawn = require('child_process').spawn;
var spawnedProcesses = [];
const meteor = spawn('meteor', ['--allow-superuser']);

meteor.stdout.on('data', function(data) {
   console.log(data.toString());
   if (data.toString().match(/=> App running at: http:\/\/localhost:3000\//)) {
       console.log("Meteor started... running scripts");
       spawnedProcesses.push(meteor.pid);
       runTestScripts();
   }
});

meteor.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
});

meteor.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
});

function runTestScripts() {
    const chimp = spawn('.scripts/start_tests.sh');
    chimp.stdout.on('data', function(data) {
        console.log("[chimp]: ", data.toString())
    });
    chimp.stderr.on('data', (data) => {
        console.log(`[chimp]: ${data}`);
});
    chimp.on('close', (code) => {
        console.log(`[chimp][exit] Code: ${code}`);
        console.log("spawnedProcesses LOG", spawnedProcesses);
        meteor.kill();
});
};